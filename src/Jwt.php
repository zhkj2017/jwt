<?php
/**
 * 基于jwt方式的回话管理工具
 */

namespace Zhihuikeji\Jwt;

class Jwt
{
    // 秘钥
    static $secret = '';

    private $time_span = 7200;

    private $header = ['typ' => 'jwt', 'alg' => ''];
    private $header_base64 = '';
    private $payload = [
        'iss' => '',
        'sub' => '',
        'aud' => '',
        'iat' => '',
        'exp' => '',
        'nbf' => '',
        'data' => []
    ];
    private $payload_base64 = '';

    private $rec_token = '';
    private $rec_header = [];
    private $rec_payload = [];

    private $has_valid = false;

    function __construct($secret, $issue = '', $alg = 'HS256')
    {
        self::$secret = $secret;
        $this->header['alg'] = $alg;
        $this->payload['iss'] = $issue;
    }

    /**
     * 设置header内容
     * @param string $alg
     * @param string $typ 类型，默认：jwt
     * @return void
     */
    function set_header($alg, $typ = 'jwt')
    {
        $this->header['alg'] = $alg;
        $this->header['typ'] = $typ;
        $this->header_base64 = '';
    }

    /**
     * 设置token的有效期
     * @param $time
     * @return void
     */
    function valid_time($time)
    {
        $this->time_span = (int)$time;
    }

    function __set($property, $value)
    {
        if ($property == 'data') {
            throw new \Exception('payload不能直接设置data属性');
        }
        if (isset($this->payload[$property])) {
            if (in_array($property, ['iat', 'exp', 'nbf'])) {
                $this->payload[$property] = (int)$value;
            } else {
                $this->payload[$property] = $value;
            }
        } else {
            $this->payload['data'][$property] = $value;
        }
        $this->payload_base64 = '';
    }

    private function get_header_base64()
    {
        if (empty($this->header_base64)) {
            $this->header_base64 = rtrim(base64_encode(json_encode($this->header)), '=');
        }
        return $this->header_base64;
    }

    private function get_payload_base64()
    {
        if (empty($this->payload_base64)) {
            if (empty($this->payload['iat']) || !is_numeric($this->payload['iat'])) {
                $this->payload['iat'] = time();
            }
            if (empty($this->payload['nbf']) || !is_numeric($this->payload['nbf'])) {
                $this->payload['nbf'] = $this->payload['iat'];
            }
            if (empty($this->payload['exp']) || !is_numeric($this->payload['exp'])) {
                $this->payload['exp'] = $this->payload['iat'] + $this->time_span;
            }
            $this->payload_base64 = rtrim(base64_encode(json_encode($this->payload)), '=');
        }
        return $this->payload_base64;
    }

    private function sign($header_base64, $payload_base64, $alg)
    {
        if (empty(self::$secret)) {
            throw new \Exception('秘钥不能为空。');
        }
        if (!in_array(strtolower($alg), ['hs256', 'md5', 'sha256'])) {
            throw new \Exception('签名算法不支持。');
        }
        $str = $header_base64 . '.' . $payload_base64;
        $alg = strtolower($alg) == 'hs256' ? 'sha256' : strtolower($alg);
        return hash_hmac($alg, $str, self::$secret);
    }

    /**
     * 生成token
     * @return string
     * @throws \Exception
     */
    function get_token()
    {
        return $this->get_header_base64() . '.' . $this->get_payload_base64() . '.' . $this->sign($this->get_header_base64(), $this->get_payload_base64(), $this->header['alg']);
    }

    /**
     * 接收并存储$token
     * @param $token
     * @return void
     * @throws \Exception
     */
    function save_token($token)
    {
        if (empty($token)) {
            throw new \Exception('token不能为空');
        }
        $this->rec_token = $token;
    }

    /**
     * 校验收到的token是否正确
     * @return array|string[]
     * @throws \Exception
     */
    function validate()
    {
        if ($this->has_valid) {
            return $this->success();
        }

        if (empty($this->rec_token)) {
            return $this->error('token为空。');
        }
        try {
            $arr_token = explode('.', $this->rec_token);
            if (count($arr_token)!=3){
                throw new \Exception('token格式错误。');
            }
            $rec_header = json_decode(base64_decode($arr_token[0]), true);
            $rec_payload = json_decode(base64_decode($arr_token[1]), true);
        } catch (\Exception $e) {
            return $this->error('token格式错误。');
        }

        try {
            if (empty($rec_header['alg'])) {
                throw new \Exception('签名算法为空。');
            }
            $rec_sign = $this->sign($arr_token[0], $arr_token[1], $rec_header['alg']);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        if (!hash_equals($rec_sign, $arr_token[2])) {
            return $this->error('签名验证失败。');
        }
        $now = time();
        if (!empty($this->payload['iss']) && $this->payload['iss'] !== $rec_payload['iss']) {
            return $this->error('签发人错误。');
        }
        if (empty($rec_payload['exp']) || $rec_payload['exp'] < $now) {
            return $this->error('token已经过期。');
        }
        if (!empty($rec_payload['nbf']) && $rec_payload['nbf'] > $now) {
            return $this->error('token尚未生效。');
        }
        $this->has_valid = true;
        $this->rec_payload = $rec_payload;
        $this->rec_header = $rec_header;
        return $this->success();
    }

    private function error($msg)
    {
        return [
            'result' => 'fail',
            'msg' => $msg,
        ];
    }

    private function success($data = [])
    {
        $res = [
            'result' => 'success',
        ];
        if (!empty($data)) {
            $res['data'] = $data;
        }
        return $res;
    }

    /**
     * 从收到的token中读取负载
     * @param bool $has_data 是否显示data数据
     * @return string|null
     */
    function get_payload($has_data = false)
    {
        if ($this->has_valid) {
            if ($has_data) {
                return $this->rec_payload;
            } else {
                $temp = $this->rec_payload;
                if (isset($temp['data'])) {
                    unset($temp['data']);
                }
                return $temp;
            }
        } else {
            $valid = $this->validate();
            if ($valid['result'] !== 'success') {
                return null;
            }
            $this->has_valid = true;
            return $this->get_payload();
        }
    }

    /**
     * 从收到的token中读取数据
     * @return string|null
     */
    function get_data()
    {
        if ($this->has_valid) {
            if (isset($this->rec_payload['data'])) {
                return $this->rec_payload['data'];
            } else {
                return null;
            }
        } else {
            $valid = $this->validate();
            if ($valid['result'] !== 'success') {
                return null;
            }
            $this->has_valid = true;
            return $this->get_data();
        }
    }

    /**
     * 更新token到期时间
     * @return string
     * @throws \Exception
     */
    function renew_token()
    {
        if (!$this->has_valid) {
            throw new \Exception('更新token前请先完成验证。');
        }
        $header_base64 = explode('.', $this->rec_token)[0];
        $payload = $this->rec_payload;
        $payload['exp'] += $this->time_span;
        $payload_base64 = rtrim(base64_encode(json_encode($payload)), '=');
        $sign = $this->sign($header_base64, $payload_base64, $this->rec_header['alg']);
        return $header_base64 . '.' . $payload_base64 . '.' . $sign;
    }

}